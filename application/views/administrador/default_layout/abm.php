<?php $this->load->view('administrador/default_layout/header');?>
<!-- BEGIN PAGE -->
<div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <?php $this->load->view('administrador/dashboard/sidebar');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                   <h3 class="page-title">
                     Dashboard
                   </h3>
                   <?php $this->load->view('administrador/breadcrumb/breadcrumb.php');?>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->       
         </div>
         <div class="container-fluid">
             <?php echo $output ?>
         </div>
            <!-- END PAGE HEADER-->       
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
<!-- END PAGE -->
<!-- Button trigger modal -->
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <form action="<?php echo site_url('administrador/avisos/rechazar/motivo'); ?>" method="POST">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Motivo del Rechazo</h4>
        </div>
        <div class="modal-body">
            <p>Por favor ingrese el motivo del rechazo</p>
            <textarea name="motivo"></textarea>
        </div>
            <input type="hidden" name="aviso" id="aviso" value=""/>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
        </div><!-- /.modal-content -->
    </form>
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php $this->load->view('administrador/default_layout/footer');?>
