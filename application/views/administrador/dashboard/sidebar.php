<!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">
         <!-- BEGIN SIDEBAR MENU -->
          <ul class="sidebar-menu">
              <li class="sub-menu active">
                  <a class="" href="<?php echo site_url('administrador/dashboard') ?>">
                      <i class="icon-dashboard"></i>
                      <span>Tablero</span>
                  </a>
              </li>
              <li class="sub-menu">
                  <a href="javascript:;" class="">
                      <i class="icon-book"></i>
                      <span>Listas</span>
                      <span class="arrow"></span>
                  </a>
                  <ul class="sub">
                      <li><a class="" href="<?php echo site_url('administrador/empleados') ?>">Lista de Precio</a></li>
                      <li><a class="" href="<?php echo site_url('administrador/') ?>">ABM Articulos</a></li>
                      <li><a class="" href="<?php echo site_url('administrador/') ?>">ABM Listas</a></li>
                      <li><a class="" href="<?php echo site_url('administrador/') ?>"></a></li>
                  </ul>
              </li>
			  <li class="sub-menu">
                  <a href="javascript:;" class="">
                      <i class="icon-user"></i>
                      <span>Usuarios</span>
                      <span class="arrow"></span>
                  </a>
                  <ul class="sub">
                      <li><a class="" href="<?php echo site_url('administrador/usuarios/add') ?>">Agregar</a></li>
                      <li><a class="" href="<?php echo site_url('administrador/usuarios') ?>">Listado</a></li>
                  </ul>
              </li>
              <!--
              <li class="sub-menu">
                  <a href="javascript:;" class="">
                      <i class="icon-user"></i>
                      <span>Clientes</span>
                      <span class="arrow"></span>
                  </a>
                  <ul class="sub">
                      <li><a class="" href="<?php echo site_url('administrador/clientes') ?>">Listado</a></li>
                  </ul>
              </li>
              -->
              
          </ul>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
<!-- END SIDEBAR -->