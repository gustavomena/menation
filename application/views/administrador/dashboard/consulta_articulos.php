<?php $this->load->view('administrador/dashboard/header'); ?>
<!-- BEGIN PAGE -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('administrador/dashboard/sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->  
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        Consulta de Articulos
                    </h3>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>

            <div class="row-fluid">
                <div class="span4">
                    <div class="widget green">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i> Datos del Articulo </h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                                <a href="javascript:;" class="icon-remove"></a>
                            </span>
                        </div>
                        <div class="widget-body">
                            <form method="post" action="consulta_articulos" id="form_consulta">
								<div class="input-append search-input-area">
                                   <input class="" id="appendedInputButton" name="codigo" type="text" placeholder="Ingrese Codigo">
                                   <button class="btn" name="buscar" type="button" onclick="consultarArt();"><i class="icon-search"></i></button>
                                </div>
                                
                                <?php
                                if (isset($_POST['codigo']) && isset($articulo)) {
                                    if (count($articulo) > 0) {
                                        ?>
                                        <div class="controls">
                                            <span class="label-info" ><h4><?= $articulo['codigo']; ?></h4></span>
                                            <span class="text-info" ><h2><?= $articulo['descripcion']; ?></h2></span>
											<span class="label-info" ><h4>$<?= $articulo['precio_lista']; ?></h4></span>
                                        </div>
                                        
                                        <?php
                                    }
                                }
                                ?>
                            </form>
                        </div>
                    </div>
                </div>
				    <?php
                                if (isset($_POST['codigo']) && isset($articulo)) {
                                    if (count($articulo) > 0) {
                                        ?>
				<div class="span6"  id="detalles-articulo">
					<div class="widget red">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i> Detalles del Articulo</h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                                <a href="javascript:;" class="icon-remove"></a>
                            </span>
                        </div>
                        
						<div class="widget-body">
							<div class="bs-docs-example">
                                    <ul class="nav nav-tabs" id="myTab">
                                        <li class="active"><a data-toggle="tab" href="#comparativo">Comparativo</a></li>
                                        <li class=""><a data-toggle="tab" href="#compras">Compras</a></li>
                                        <li class=""><a data-toggle="tab" href="#stock">Stock</a></li>
                                    </ul>
                                    <div class="tab-content" id="myTabContent">
                                        <div id="comparativo" class="tab-pane fade active in">
                                           <table class="table table-striped">
												<thead>
												<tr>
													<th>CODIGO</th>
													<th>PROVEEDOR</th>
													<th>PRECIO</th>
													
												</tr>
												</thead>
												<tbody>
												<tr>
													<td>3</td>
													<td>BALSAMO</td>
													<td>100</td>
												</tr>
												</tbody>
											</table>
                                        </div>
                                        <div id="compras" class="tab-pane fade">
                                            <table class="table table-striped">
												<thead>
												<tr>
													<th>CODIGO</th>
													<th>PROVEEDOR</th>
													<th>PRECIO</th>
													
												</tr>
												</thead>
												<tbody>
												<tr>
													<td>3</td>
													<td>BALSAMO</td>
													<td>100</td>
												</tr>
												</tbody>
											</table>
                                        </div>
										 <div id="stock" class="tab-pane fade">
                                            <table class="table table-striped">
												<thead>
												<tr>
													<th>Sucursal</th>
													<th>Stock</th>
												</tr>
												</thead>
												<tbody>
												<tr>
													<td>3</td>
													<td>100</td>
												</tr>
												</tbody>
											</table>
                                        </div>
                                    </div>
                            </div>
						</div>
						
					</div>
            </div>
			
			                         
				<div class="span2"  id="foto-articulo">
					<div class="widget red">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i> Foto</h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                                <a href="javascript:;" class="icon-remove"></a>
                            </span>
                        </div>
						<div class="widget-body">
							<img src="http://www.balsamo.com.ar/aplicacion/infobal/Imagenes/11088.JPG" >
						</div>
					</div>
				</div>
			    
                                        <?php
                                    }
                                }
                                ?>

        </div>
    </div>
    <!-- END PAGE -->  
</div>

<script>
	function consultarArt(){
		//$('#detalles-articulo').attr('style','display:bock');
		$('#form_consulta').submit();
	}
</script>

<!-- END PAGE -->
<?php $this->load->view('administrador/dashboard/footer'); ?>
