<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['administrador'] = 'user_admin_controller/index';

/* Usuarios */
$route['administrador/usuarios'] = 'user_admin_controller/usuarios';
$route['administrador/usuarios/add'] = 'user_admin_controller/usuarios';
$route['administrador/usuarios/insert'] = 'user_admin_controller/usuarios';
$route['administrador/usuarios/success/(:num)'] = 'user_admin_controller/usuarios';
$route['administrador/usuarios/insert_validation'] = 'user_admin_controller/usuarios';
$route['administrador/usuarios/ajax_list_info'] = 'user_admin_controller/usuarios';
$route['administrador/usuarios/ajax_list'] = 'user_admin_controller/usuarios';
$route['administrador/usuarios/read/(:num)'] = 'user_admin_controller/usuarios';
$route['administrador/usuarios/edit/(:num)'] = 'user_admin_controller/usuarios';
$route['administrador/usuarios/update/(:num)'] = 'user_admin_controller/usuarios';
$route['administrador/usuarios/update_validation/(:num)'] = 'user_admin_controller/usuarios';
$route['administrador/usuarios/delete/(:num)'] = 'user_admin_controller/usuarios';
$route['administrador/usuarios/export'] = 'user_admin_controller/usuarios';
$route['administrador/usuarios/print'] = 'user_admin_controller/usuarios';
$route['administrador/usuarios'] = 'user_admin_controller/usuarios';


/* Clientes */
$route['administrador/clientes'] = 'customer_controller/manager_clientes';
$route['administrador/clientes/add'] = 'customer_controller/manager_clientes';
$route['administrador/clientes/insert'] = 'customer_controller/manager_clientes';
$route['administrador/clientes/success/(:num)'] = 'customer_controller/manager_clientes';
$route['administrador/clientes/insert_validation'] = 'customer_controller/manager_clientes';
$route['administrador/clientes/ajax_list_info'] = 'customer_controller/manager_clientes';
$route['administrador/clientes/ajax_list'] = 'customer_controller/manager_clientes';
$route['administrador/clientes/read/(:num)'] = 'customer_controller/manager_clientes';
$route['administrador/clientes/edit/(:num)'] = 'customer_controller/manager_clientes';
$route['administrador/clientes/update/(:num)'] = 'customer_controller/manager_clientes';
$route['administrador/clientes/update_validation/(:num)'] = 'customer_controller/manager_clientes';
$route['administrador/clientes/delete/(:num)'] = 'customer_controller/manager_clientes';
$route['administrador/clientes/export'] = 'customer_controller/manager_clientes';
$route['administrador/clientes/print'] = 'customer_controller/manager_clientes';
$route['administrador/clientes'] = 'customer_controller/manager_clientes';



/* categorias */
$route['administrador/categorias/add'] = 'category_controller/manager_category';
$route['administrador/categorias/insert'] = 'category_controller/manager_category';
$route['administrador/categorias/success/(:num)'] = 'category_controller/manager_category';
$route['administrador/categorias/insert_validation'] = 'category_controller/manager_category';
$route['administrador/categorias/ajax_list_info'] = 'category_controller/manager_category';
$route['administrador/categorias/ajax_list'] = 'category_controller/manager_category';
$route['administrador/categorias/read/(:num)'] = 'category_controller/manager_category';
$route['administrador/categorias/edit/(:num)'] = 'category_controller/manager_category';
$route['administrador/categorias/update/(:num)'] = 'category_controller/manager_category';
$route['administrador/categorias/update_validation/(:num)'] = 'category_controller/manager_category';
$route['administrador/categorias/delete/(:num)'] = 'category_controller/manager_category';
$route['administrador/categorias/export'] = 'category_controller/manager_category';
$route['administrador/categorias/print'] = 'category_controller/manager_category';
$route['administrador/categorias'] = 'category_controller/manager_category';
$route['administrador/categorias/upload_file/image_tile_url'] = 'category_controller/manager_category';
$route['administrador/categorias/delete_file/image_tile_url/'] = 'category_controller/manager_category';
$route['administrador/categorias/(:any)'] = 'category_controller/manager_category';

/* Sub categorias */
$route['administrador/sub-categorias/add'] = 'subcategory_controller/manager_subcategory';
$route['administrador/sub-categorias/insert'] = 'subcategory_controller/manager_subcategory';
$route['administrador/sub-categorias/success/(:num)'] = 'subcategory_controller/manager_subcategory';
$route['administrador/sub-categorias/insert_validation'] = 'subcategory_controller/manager_subcategory';
$route['administrador/sub-categorias/ajax_list_info'] = 'subcategory_controller/manager_subcategory';
$route['administrador/sub-categorias/ajax_list'] = 'subcategory_controller/manager_subcategory';
$route['administrador/sub-categorias/read/(:num)'] = 'subcategory_controller/manager_subcategory';
$route['administrador/sub-categorias/edit/(:num)'] = 'subcategory_controller/manager_subcategory';
$route['administrador/sub-categorias/update/(:num)'] = 'subcategory_controller/manager_subcategory';
$route['administrador/sub-categorias/update_validation/(:num)'] = 'subcategory_controller/manager_subcategory';
$route['administrador/sub-categorias/delete/(:num)'] = 'subcategory_controller/manager_subcategory';
$route['administrador/sub-categorias/export'] = 'subcategory_controller/manager_subcategory';
$route['administrador/sub-categorias/print'] = 'subcategory_controller/manager_subcategory';
$route['administrador/sub-categorias/upload_file/image_tile_url'] = 'subcategory_controller/manager_subcategory';
$route['administrador/sub-categorias/delete_file/image_tile_url/'] = 'subcategory_controller/manager_subcategory';
$route['administrador/sub-categorias/(:any)'] = 'subcategory_controller/manager_subcategory';
$route['administrador/sub-categorias'] = 'subcategory_controller/manager_subcategory';

/* configuracion */
$route['administrador/configuracion/add'] = 'configuration_controller/manager_configuration';
$route['administrador/configuracion/insert'] = 'configuration_controller/manager_configuration';
$route['administrador/configuracion/success/(:num)'] = 'configuration_controller/manager_configuration';
$route['administrador/configuracion/insert_validation'] = 'configuration_controller/manager_configuration';
$route['administrador/configuracion/ajax_list_info'] = 'configuration_controller/manager_configuration';
$route['administrador/configuracion/ajax_list'] = 'configuration_controller/manager_configuration';
$route['administrador/configuracion/read/(:num)'] = 'configuration_controller/manager_configuration';
$route['administrador/configuracion/edit/(:num)'] = 'configuration_controller/manager_configuration';
$route['administrador/configuracion/update/(:num)'] = 'configuration_controller/manager_configuration';
$route['administrador/configuracion/update_validation/(:num)'] = 'configuration_controller/manager_configuration';
$route['administrador/configuracion/delete/(:num)'] = 'configuration_controller/manager_configuration';
$route['administrador/configuracion/export'] = 'configuration_controller/manager_configuration';
$route['administrador/configuracion/print'] = 'configuration_controller/manager_configuration';
$route['administrador/configuracion'] = 'configuration_controller/manager_configuration';
$route['administrador/configuracion/upload_file/image_tile_url'] = 'configuration_controller/manager_configuration';
$route['administrador/configuracion/delete_file/image_tile_url/'] = 'configuration_controller/manager_configuration';
$route['administrador/configuracion/(:any)'] = 'category_controller/manager_configuration';

/* Roles */
$route['administrador/roles/add'] = 'rol_controller/manager_rol';
$route['administrador/roles/insert'] = 'rol_controller/manager_rol';
$route['administrador/roles/success/(:num)'] = 'rol_controller/manager_rol';
$route['administrador/roles/insert_validation'] = 'rol_controller/manager_rol';
$route['administrador/roles/ajax_list_info'] = 'rol_controller/manager_rol';
$route['administrador/roles/ajax_list'] = 'rol_controller/manager_rol';
$route['administrador/roles/read/(:num)'] = 'rol_controller/manager_rol';
$route['administrador/roles/edit/(:num)'] = 'rol_controller/manager_rol';
$route['administrador/roles/update/(:num)'] = 'rol_controller/manager_rol';
$route['administrador/roles/update_validation/(:num)'] = 'rol_controller/manager_rol';
$route['administrador/roles/delete/(:num)'] = 'rol_controller/manager_rol';
$route['administrador/roles/export'] = 'rol_controller/manager_rol';
$route['administrador/roles/print'] = 'rol_controller/manager_rol';
$route['administrador/roles'] = 'rol_controller/manager_rol';
$route['administrador/roles/upload_file/image_tile_url'] = 'rol_controller/manager_rol';
$route['administrador/roles/delete_file/image_tile_url/'] = 'rol_controller/manager_rol';
$route['administrador/roles/(:any)'] = 'rol_controller/manager_rol';



/* User Manager */
$route['registracion'] = 'user_manager_controller/register';
$route['activate'] = 'user_manager_controller/activate';
$route['ingresar'] = 'user_manager_controller/login';
$route['salir'] = 'user_manager_controller/logout'; 
$route['perfil'] = 'user_manager_controller/show_profile';
$route['editar-perfil'] = 'user_manager_controller/edit_profile';
$route['cambiar-password'] = 'user_manager_controller/reset_pass';
$route['reiniciar-password'] = 'user_manager_controller/reset';

/* Dashboard */
$route['administrador/dashboard'] = 'dashboard_controller/index';

/*Consulta Articulos*/
$route['administrador/consulta_articulos'] = 'articulo_controller/consulta_articulos';
$route['administrador/consulta_articulos/add'] = 'articulo_controller/consulta_articulos';
$route['administrador/consulta_articulos/insert'] = 'articulo_controller/consulta_articulos';
$route['administrador/consulta_articulos/success/(:num)'] = 'articulo_controller/consulta_articulos';
$route['administrador/consulta_articulos/insert_validation'] = 'articulo_controller/consulta_articulos';
$route['administrador/consulta_articulos/ajax_list_info'] = 'articulo_controller/consulta_articulos';
$route['administrador/consulta_articulos/ajax_list'] = 'articulo_controller/consulta_articulos';
$route['administrador/consulta_articulos/read/(:num)'] = 'articulo_controller/consulta_articulos';
$route['administrador/consulta_articulos/edit/(:num)'] = 'articulo_controller/consulta_articulos';
$route['administrador/consulta_articulos/update/(:num)'] = 'articulo_controller/consulta_articulos';
$route['administrador/consulta_articulos/update_validation/(:num)'] = 'articulo_controller/consulta_articulos';
$route['administrador/consulta_articulos/delete/(:num)'] = 'articulo_controller/consulta_articulos';
$route['administrador/consulta_articulos/export'] = 'articulo_controller/consulta_articulos';
$route['administrador/consulta_articulos/print'] = 'articulo_controller/consulta_articulos';
$route['administrador/consulta_articulos/upload_file/image_tile_url'] = 'articulo_controller/consulta_articulos';
$route['administrador/consulta_articulos/delete_file/image_tile_url/'] = 'articulo_controller/consulta_articulos';
$route['administrador/consulta_articulos/(:any)'] = 'articulo_controller/consulta_articulos';

/*ABM Articulos*/
$route['administrador/abm_articulos'] = 'articulo_controller/abm_articulos';
$route['administrador/abm_articulos/add'] = 'articulo_controller/abm_articulos';
$route['administrador/abm_articulos/insert'] = 'articulo_controller/abm_articulos';
$route['administrador/abm_articulos/success/(:num)'] = 'articulo_controller/abm_articulos';
$route['administrador/abm_articulos/insert_validation'] = 'articulo_controller/abm_articulos';
$route['administrador/abm_articulos/ajax_list_info'] = 'articulo_controller/abm_articulos';
$route['administrador/abm_articulos/ajax_list'] = 'articulo_controller/abm_articulos';
$route['administrador/abm_articulos/read/(:num)'] = 'articulo_controller/abm_articulos';
$route['administrador/abm_articulos/edit/(:num)'] = 'articulo_controller/abm_articulos';
$route['administrador/abm_articulos/update/(:num)'] = 'articulo_controller/abm_articulos';
$route['administrador/abm_articulos/update_validation/(:num)'] = 'articulo_controller/abm_articulos';
$route['administrador/abm_articulos/delete/(:num)'] = 'articulo_controller/abm_articulos';
$route['administrador/abm_articulos/export'] = 'articulo_controller/abm_articulos';
$route['administrador/abm_articulos/print'] = 'articulo_controller/abm_articulos';
$route['administrador/abm_articulos/upload_file/image_tile_url'] = 'articulo_controller/abm_articulos';
$route['administrador/abm_articulos/delete_file/image_tile_url/'] = 'articulo_controller/abm_articulos';
$route['administrador/abm_articulos/(:any)'] = 'articulo_controller/abm_articulos';

/*Listas de precios*/
$route['administrador/abm_listas'] = 'lista_precio_controller/abm_listas';
$route['administrador/abm_listas/add'] = 'lista_precio_controller/abm_listas';
$route['administrador/abm_listas/insert'] = 'lista_precio_controller/abm_listas';
$route['administrador/abm_listas/success/(:num)'] = 'lista_precio_controller/abm_listas';
$route['administrador/abm_listas/insert_validation'] = 'lista_precio_controller/abm_listas';
$route['administrador/abm_listas/ajax_list_info'] = 'lista_precio_controller/abm_listas';
$route['administrador/abm_listas/ajax_list'] = 'lista_precio_controller/abm_listas';
$route['administrador/abm_listas/read/(:num)'] = 'lista_precio_controller/abm_listas';
$route['administrador/abm_listas/edit/(:num)'] = 'lista_precio_controller/abm_listas';
$route['administrador/abm_listas/update/(:num)'] = 'lista_precio_controller/abm_listas';
$route['administrador/abm_listas/update_validation/(:num)'] = 'lista_precio_controller/abm_listas';
$route['administrador/abm_listas/delete/(:num)'] = 'lista_precio_controller/abm_listas';
$route['administrador/abm_listas/export'] = 'lista_precio_controller/abm_listas';
$route['administrador/abm_listas/print'] = 'lista_precio_controller/abm_listas';
$route['administrador/abm_listas/upload_file/image_tile_url'] = 'lista_precio_controller/abm_listas';
$route['administrador/abm_listas/delete_file/image_tile_url/'] = 'lista_precio_controller/abm_listas';
$route['administrador/abm_listas/(:any)'] = 'lista_precio_controller/abm_listas';


/*ABM Proveedores*/
$route['administrador/abm_proveedores'] = 'proveedor_controller/abm_proveedores';
$route['administrador/abm_proveedores/add'] = 'proveedor_controller/abm_proveedores';
$route['administrador/abm_proveedores/insert'] = 'proveedor_controller/abm_proveedores';
$route['administrador/abm_proveedores/success/(:num)'] = 'proveedor_controller/abm_proveedores';
$route['administrador/abm_proveedores/insert_validation'] = 'proveedor_controller/abm_proveedores';
$route['administrador/abm_proveedores/ajax_list_info'] = 'proveedor_controller/abm_proveedores';
$route['administrador/abm_proveedores/ajax_list'] = 'proveedor_controller/abm_proveedores';
$route['administrador/abm_proveedores/read/(:num)'] = 'proveedor_controller/abm_proveedores';
$route['administrador/abm_proveedores/edit/(:num)'] = 'proveedor_controller/abm_proveedores';
$route['administrador/abm_proveedores/update/(:num)'] = 'proveedor_controller/abm_proveedores';
$route['administrador/abm_proveedores/update_validation/(:num)'] = 'proveedor_controller/abm_proveedores';
$route['administrador/abm_proveedores/delete/(:num)'] = 'proveedor_controller/abm_proveedores';
$route['administrador/abm_proveedores/export'] = 'proveedor_controller/abm_proveedores';
$route['administrador/abm_proveedores/print'] = 'proveedor_controller/abm_proveedores';
$route['administrador/abm_proveedores/upload_file/image_tile_url'] = 'proveedor_controller/abm_proveedores';
$route['administrador/abm_proveedores/delete_file/image_tile_url/'] = 'proveedor_controller/abm_proveedores';
$route['administrador/abm_proveedores/(:any)'] = 'proveedor_controller/abm_proveedores';



$route['default_controller'] = "user_manager_controller/login";
$route['404_override'] = '';


/* End of file routes.php */
/* Location: ./application/config/routes.php */